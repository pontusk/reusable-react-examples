import {
  Tab as ReachTab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
} from "@reach/tabs";
import "@reach/tabs/styles.css";
import "./index.css";
import { FC } from "react";

interface Props {
  labels: string[];
}

const Container: FC<Props> = ({ labels, children }) => (
  <Tabs>
    <TabList>
      {labels.map((label: string) => (
        <ReachTab key={label}>{label}</ReachTab>
      ))}
    </TabList>
    <TabPanels>{children}</TabPanels>
  </Tabs>
);

const Tab = {
  Container,
  Panel: TabPanel,
};

export default Tab;
