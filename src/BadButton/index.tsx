import { FC } from "react";
import "./index.css";

interface Props {}

const Button: FC<Props> = ({ children }) => <button>{children}</button>;

export default Button;
