import "./App.css";
import Tab from "./Tabs";
import BadButton from "./BadButton";
import GoodButton from "./GoodButton";
import Lorem from "./Lorem";

const App = () => {
  return (
    <main>
      <h1>Reusable React Components</h1>

      <section>
        <h2>Styling</h2>

        <h3>Bad</h3>
        <Tab.Container labels={["Flex column", "Flex row", "Grid"]}>
          <Tab.Panel>
            <Lorem />
            <div className="flex column">
              <BadButton>Button</BadButton>
              <BadButton>Button</BadButton>
            </div>
          </Tab.Panel>
          <Tab.Panel>
            <Lorem />
            <div className="flex row">
              <BadButton>Button</BadButton>
              <BadButton>Button</BadButton>
            </div>
          </Tab.Panel>
          <Tab.Panel>
            <Lorem />
            <div className="grid auto">
              <BadButton>Button</BadButton>
              <BadButton>Button</BadButton>
            </div>
          </Tab.Panel>
        </Tab.Container>

        <h3>Good</h3>
        <Tab.Container labels={["Flex column", "Flex row", "Grid"]}>
          <Tab.Panel>
            <Lorem />
            <div className="flex column padding-top align-center">
              <GoodButton className="ex-1__button">Button</GoodButton>
              <GoodButton className="ex-1__button">Button</GoodButton>
            </div>
          </Tab.Panel>
          <Tab.Panel>
            <Lorem />
            <div className="flex row padding-top">
              <GoodButton className="ex-2__button">Button</GoodButton>
              <GoodButton className="ex-2__button">Button</GoodButton>
            </div>
          </Tab.Panel>
          <Tab.Panel>
            <Lorem />
            <div className="grid auto padding-top">
              <GoodButton>Button</GoodButton>
              <GoodButton>Button</GoodButton>
            </div>
          </Tab.Panel>
        </Tab.Container>
      </section>

      <section>
        <h2>State</h2>
        <a
          rel="noreferrer"
          target="_blank"
          href="https://github.com/reach/reach-ui/blob/main/packages/tabs/src/index.tsx"
        >
          Link
        </a>
      </section>

      <section>
        <h2>Storybook</h2>
        <a rel="noreferrer" target="_blank" href="localhost:9001">
          Link
        </a>
      </section>
    </main>
  );
};

export default App;
