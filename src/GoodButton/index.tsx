import { FC } from "react";
import "./index.css";

interface Props {
  className?: string;
}

const Button: FC<Props> = ({ className = "", children }) => (
  <button className={`good-button ${className}`}>{children}</button>
);

export default Button;
